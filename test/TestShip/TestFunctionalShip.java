package TestShip;

import org.junit.jupiter.api.Test;

import flotte.Escorteur;
import flotte.Ship;
import jeu.Tableau;

public class TestFunctionalShip {

	@Test
	public void classShip() {
		Tableau tableau = new Tableau(10, "testeur");
		Escorteur hship1 = new Escorteur(2);
		hship1.locateBattle(2, 2, true);
		
		tableau.checkInTable(hship1);
		
		Escorteur hship2 = new Escorteur(2);
		hship2.locateBattle(2, 2, false);
	}
}
