package flotte;

import jeu.Tableau;

public class Ship {


	private boolean bHorizontal;
	private static int EMPTY_VALUE = 0;
	private final int MAX_SIZE = 5;
	private Element [] battleTable;

	public Ship() {
		
	}
	
	//!\Impossible de cr�er un bateau directement � partir de cette classe
	protected Ship( int size) {
		if (size>MAX_SIZE || size<1) {
			System.err.println("Error : Battle Size");
		}
		else {
			//the battle is a table of elements
			this.battleTable = new Element[size];
		} 
		
	} 


	protected boolean isbHorizontal() {
		return bHorizontal;
	}


	public void setbHorizontal(boolean bHorizontal) {
		this.bHorizontal = bHorizontal;
	}


	public Element[] getTabElementBateau() {
		return battleTable;
	}


	//Supprimer d�finitivement un bateau, en cas d'erreur avec la taille du constructeur
	protected Ship dropBateau(Ship hBateau) {
		hBateau=null;
		System.out.println("Battle drops");
		return hBateau;
	}


	//D�terminer la position de chaque �l�ment du bateau
	public void locateBattle(int x, int y, boolean bHorizontal) {
		int updateX = x;
		int updateY = y ;
		for (int i=0 ; i < this.battleTable.length ; i ++) {
				if(bHorizontal){
					battleTable[i]=new Element(updateX, updateY, this);
					System.out.println(i + "=> [" + updateX +", "+ updateY + "]");
					updateY++;
				}
				
				if(!bHorizontal){
					battleTable[i]=new Element(updateX, updateY, this);
					System.out.println(i + "=>[" + updateX +", "+ updateY+ "]");
					updateX++;
				}
		} 
	}


	



	public int isTouch(Ship hB) {
				boolean test;
				int score;
				int nbElementTouch = 0 ; 
				int nbElementNoTouch = 0; 
		
				for(int i = 0; i<hB.battleTable.length ; i++){
							test = hB.battleTable[i].isbTouche();
							if (test == true)
								nbElementTouch++;
							else
								nbElementNoTouch++;
				}
				
				//To give the score
				if (nbElementTouch == 1)
					score= 2;
				if (nbElementNoTouch == 0) {
					System.out.println("ship touched !");
					score= 3;
				}else
					score= 1;
		
				return score;
	}




	//Attribuer les nouveaux coordonn�s au bateau pour avancer 
	public Ship goTo(int depx, int depy) {
		int t0x = battleTable[0].getPositionX();
		int t0y = battleTable[0].getPositionY();

		

		//Si sup�rieurs � x, les �l�ments avancent d'une case
		if (depx>t0x) {
			for (int i = 0 ; i< battleTable.length; i++) { 
				battleTable[i].setPositionX(battleTable[i].getPositionX()+1);
			}
		}
		//Si inf�rieur � x, les �l�ments reculent d'une case
		if (depx<t0x) {
			for (int i = 0 ; i< battleTable.length; i++) {
				battleTable[i].setPositionX(battleTable[i].getPositionX()-1);
			}
		}



		//Si sup�rieur � y, les �l�ments du bateau montent d'une case
		if (depy>t0y) {
			for (int i = 0 ; i< battleTable.length; i++) {
				battleTable[i].setPositionY(battleTable[i].getPositionY()+1);
			}
		}
		//Si inf�rieur � y, les �l�ments du bateau descendent d'une case
		if (depy<t0y) {
			for (int i = 0 ; i< battleTable.length; i++) {
				battleTable[i].setPositionY(battleTable[i].getPositionY()-1);
			}
		}
		return this;
	
	}


}