package flotte;

public class Element {

	private int positionX;
	private int positionY;
	private boolean bTouche = false; 
	private Ship hBateau;


	//Conna�tre la place bateau du bateau pour avancer
	public Element(int positionX, int positionY, Ship hBateau) {
		this.positionX= positionX;
		this.positionY= positionY;
		this.hBateau= hBateau;
	}

	public int getPositionX() {
		return positionX;
	}

	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}

	public int getPositionY() {
		return positionY;
	}

	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}


	public Ship gethBateau() {
		return hBateau;
	}

	public boolean isbTouche() {
		return bTouche;
	}

	public void setbTouche(boolean bTouche) {
		this.bTouche = bTouche;
	}


	

	public boolean estTouche(int px, int py) {
		this.positionX = px;
		this.positionY = py;
		if (bTouche == true){
			return bTouche;
		}
		else {
			bTouche = true;
			System.out.println(bTouche);
			return bTouche;
		}
			
	}


}
