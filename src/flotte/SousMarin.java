package flotte;

import jeu.Tableau;
import utilitaire.Message;

public class SousMarin extends Ship {

	private  boolean bEnPlongee; //true = en plong�e
	private final int NBELEMENT = 1;
	private Message text;
	
	
	
	public SousMarin(int NBELEMENT) {
		super(NBELEMENT);
	}


	public boolean modifierEtat(boolean bEnPlongee) {
		this.bEnPlongee= bEnPlongee;
		return this.bEnPlongee;
	}
	

	public boolean isbEnPlongee() {
		return bEnPlongee;
	}

	
	
	public Ship avancer(int depx, int depy) {
		if (bEnPlongee == false) {
			super.goTo(depx, depy);
			return this ;
		}
		else
			return this ;
	}

	public int estTouche() {
		bEnPlongee = isbHorizontal();
		System.out.println(bEnPlongee);
		if (bEnPlongee == false) {
			System.out.println("affected submarine");
			return 3;
		}	
			else {
				System.err.println("Diving submarine: shot dodged!");
				return 0;
			}

	}
}


