package jeu;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

import flotte.Ship;
import flotte.Croiseur;
import flotte.Escorteur;
import flotte.SousMarin;
import utilitaire.Message;
import flotte.Element;

public class Tableau {
		public static Scanner sc = new Scanner(System.in);
		private Message text= new Message();
		public Element hElement;
		private final int DIMENSSION;
		private final static int  BEGIN_VALUE_OF_TABLE = 0;
		private ArrayList<Ship> shipList;
		private int point;
		private String n_player;
	
	
	
		public Tableau(int dimenssion, String aName_player) {
			DIMENSSION = dimenssion;
			n_player = aName_player;
			shipList = new ArrayList <Ship>();
		}
	
	
		public int getDIMENSSION() {
			return DIMENSSION;
		}
	
	
		public String getPlayer() {
			return n_player;
		}
	
	
		public void setPlayer(String n_player) {
			this.n_player = n_player;
		}
		
		
		public ArrayList<Ship> getShipList() {
			return shipList;
		}
	
	
		public void setPoint(int point) {
			this.point = point;
		}
	
		public int getPoint() {
			return point;
		}


		//M�thode pour ajouter les bateaux � la liste de la grille
		public boolean ajouterBateau(Ship hBateau) {
				boolean test = verifBateauColl(hBateau);
				
				if (test=true) {
							shipList.add(hBateau);
				}
				return test;
		}



		//v�rifi� les classes des objets de la collection
		public boolean verifBateauColl(Ship hBateau) {
			boolean checkProcess = false;
	
			for (Ship x : shipList) {
				if (x instanceof Escorteur || x instanceof SousMarin || x instanceof Croiseur) {
					checkProcess = true;
				}
					
			}
			return checkProcess;
		}



		//Afficher la grille
		public void afficherTableau() {
			int test;
	
			for (int i=0; i<=DIMENSSION; i++) {
				for (int j=0; j<=DIMENSSION; j++) {
					if (i==0) {
						if (j==DIMENSSION)
							System.out.print(" "+ j + " ");
						else
							System.out.print("  "+ j + " ");
					}
					else if (j==0) {
						if (j==DIMENSSION)
							System.out.print(" "+ i + "");
						else if	((j==0)&&(i==DIMENSSION))
							System.out.print(""+ i + "  ");
						else
							System.out.print(" "+ i + "  ");
					}
	
	
					else {
						test= checkCoordonnerBateau(j, i);
						if (test==1) 
							System.out.print(" xXx");
						else
							System.out.print("  - ");
					}
				}
				System.out.println(""); // faire le retour � la ligne
			}
		}


		public int checkCoordonnerBateau(int x, int y) {
			for (Ship b : shipList) {
				for (Element e : b.getTabElementBateau()) {
					if ((e.getPositionX()== x) && (e.getPositionY()==y))
						return 1;
				}
			}
			return 0;
		}



	
		public boolean isFreeSpace(Ship hBateau) {
			int eX;
			int eY;
			boolean isFree = true;
			
			for (Element e : hBateau.getTabElementBateau()) {
				eX = e.getPositionX();
				eY = e.getPositionY();
	
				for (Ship b : shipList) {
					if (b != hBateau) {
						for (Element unite : b.getTabElementBateau()) {
							if (unite.getPositionX()==eX && unite.getPositionY()==eY) {
								isFree=false;
							}
						}
					}
				}
			}
			return isFree;
		}


	
		public int effectuerCoup(int px, int py) {
			int coup;
			boolean test;
	
			if (px<1 || px>DIMENSSION || py<1 || py>DIMENSSION)
				setPoint(getPoint()-1);
	
			for (Ship b : shipList) {
	
				for (Element unite : b.getTabElementBateau()) {
					if (unite.getPositionX()==px && unite.getPositionY()==py) {
						test= unite.estTouche(px, py);
	
						//Dans le cas d'un sous-marin
						if (b instanceof SousMarin) {
							coup = ((SousMarin) b).estTouche();
							setPoint(getPoint()+coup);
						}
						else {
							coup = b.isTouch(b);
							setPoint(getPoint()+coup);
							System.out.println("les points du J : " + getPoint());
						}
	
						if (coup ==3)
							shipList.remove(b);
					}
				} 
			}
			return getPoint();
		}



		/*M�thode pour v�rifier qu'il y est au moins une case de s�paration entre deux bateaux.
		 *Le bateau pass� en param�tre correspond � un clone pour v�rifier la position. */
		public boolean respectSpace(Ship hBateau) {
			int eX, eY;
			boolean checkProcess;
			Element[] testShip= hBateau.getTabElementBateau();
			
			//D�limitation du p�rim�tre par rapport � la position du bateau
			int Xmin = testShip[0].getPositionX() - 1;
			int Xmax = testShip[testShip.length-1].getPositionX() + 1;
			int Ymin = testShip[0].getPositionY() - 1;
			int Ymax = testShip[testShip.length-1].getPositionY() + 1;
	
			//Clonnage de la collection
			ArrayList <Ship> collectionTest = (ArrayList<Ship>) shipList.clone();
	
			//O� je supprime le bateau � tester
			checkProcess =collectionTest.remove(hBateau);
	
			//Initialisation
			checkProcess =true;
	
			// Parcours la collection clonn�e pour v�rifier si un bateau est dans le p�rim�tre
			for (Ship b : collectionTest) {
				for( Element e : b.getTabElementBateau()) {
					eX = e.getPositionX();
					eY = e.getPositionY();
	
					if (eX <=Xmax && eX>= Xmin) {
						if (eY <=Ymax && eY>=Ymin) {
							checkProcess=false;
						}
					}
				}
			}
			//Supprimer la collection clon�e
			collectionTest.isEmpty();
			System.gc();
	
			return checkProcess; 
		}


		//Construction et insertion des bateaux dans la liste
		public ArrayList <Ship> insertBateau(){
			Croiseur hCroiseur = new Croiseur(3);
			Escorteur hEscorteur = new Escorteur(2);
			SousMarin hSM = new SousMarin(1);
	
			this.ajouterBateau(hCroiseur);
			this.ajouterBateau(hEscorteur);
			this.ajouterBateau(hSM);
	
			System.out.println(text.M3);
	
			return this.shipList;
		}


		//Disposition de tous les bateaux d'un joueur sur la grille (au d�but)
		public ArrayList <Ship> installation() {
			boolean isInGrill;
			
			List<Ship> updatedshipList = (List<Ship>) shipList.clone();
			shipList.clear();
			
			for (Ship currentShip : updatedshipList) {
				int x;
				int y;
				String position;
				isInGrill = false;
				
				do {	
						//valeur x
						System.out.println("#" + currentShip+ " - " + text.M6);
						x = sc.nextInt();
							
						//valeur y
						System.out.println(text.M7);
						y = sc.nextInt();
								
						//valeur horizontal
						do {
	
								System.out.println(text.M8);
								position = sc.next().toUpperCase();
						} while(!position.equals("FALSE") && !position.equals("TRUE"));
					
						currentShip.locateBattle(x, y, Boolean.valueOf(position));
							
						isInGrill = checkInTable(currentShip);
						
						if(isInGrill) {
							isInGrill = isFreeSpace(currentShip);
						}
						
						if(isInGrill) {
							isInGrill = respectSpace(currentShip);
						}
				} while(!isInGrill);
				
				shipList.add(currentShip);
			}
	
			return shipList;
		}
	
		
		//M�thode v�rifiant s'il est bien dans la d�limitation
		public  boolean checkInTable(Ship hShip) {
			boolean isValid = checkX(hShip);
			
			if(isValid) {
				isValid = checkY(hShip);
			}
			
			return isValid ;
		}


		private boolean checkX(Ship hShip) {
			boolean isCheck = true;
			
			for (int i=0; i<hShip.getTabElementBateau().length; i++) {
						Element hElement= hShip.getTabElementBateau()[i];
						int x= hElement.getPositionX();
		
						if (x>DIMENSSION || x<=BEGIN_VALUE_OF_TABLE) {
							System.err.println("Error : locate [x] is outside => " + x) ;
							isCheck = false ;
						}
			}
			return isCheck;
		}

		
		private boolean checkY(Ship hShip) {
			boolean isCheck = true;
			
			for (int i=0; i<hShip.getTabElementBateau().length; i++) {
							Element hElement= hShip.getTabElementBateau()[i];
							int y= hElement.getPositionY();
							
							if (y>DIMENSSION || y<=BEGIN_VALUE_OF_TABLE){
									System.err.println("Error : locate [y] is outside => " + y);
									isCheck = false;
							}
			}
			return isCheck;
		}
	
		
		public int moveBateau(Ship hBateau, int depx, int depy) {
			boolean isInGrill;
			int score = 0;
			Ship bateauClone = hBateau;
			
			afficherTableau();
			
			bateauClone = bateauClone.goTo(depx, depy);
			
			isInGrill = checkInTable(hBateau);	
			
			if(isInGrill) {
				isInGrill = isFreeSpace(bateauClone);
			}
			
			if(isInGrill) {
				isInGrill = respectSpace(bateauClone);
			}
			
			if (isInGrill) {
				hBateau = bateauClone;
				score = 1;
			}
		
			return score;
		}
	

}

