package principal;

import java.util.Scanner;
import flotte.Ship;
import flotte.Croiseur;
import flotte.Escorteur;
import flotte.PorteAvions;
import flotte.SousMarin;
import jeu.Tableau;
import utilitaire.Message;


public class Lanceur {

	public static Scanner sc = new Scanner(System.in);
	public static Message text= new Message();
	private static int DIMENSSION ;
	private static Tableau grille_P1;
	private static Tableau grille_P2;
	private static int nb_saisie;
	private static int nb_saisie_x;
	private static int nb_saisie_y;


	public static void main(String[] args) {

		//Mise en place du Joueur1
		System.out.println(text.M1);
		String saisie = sc.next();	

		//Determiner taille de la grille
		System.out.println(text.M2);
		nb_saisie= sc.nextInt();
		DIMENSSION = nb_saisie;
		grille_P1= new Tableau(DIMENSSION, saisie);


		//Installation par d�faut des bateaux
		grille_P1.insertBateau();

		//Possibilite exceptionnelle d'instancier un porte-avions pour le Joueur1
		nb_saisie= Message.questionF(text.M4);
		
		if (nb_saisie== 1) {
					PorteAvions hPA = new PorteAvions(5);
					boolean isPosition = grille_P1.verifBateauColl(hPA);
					
					if (!isPosition) {
						grille_P1.ajouterBateau(hPA);
					}
					
					System.out.println(text.M10);
				
		} else {
			System.out.println(text.M11);
		}

		//Choisir les coordonn�s de chaque bateau sur la grille_P1
		grille_P1.installation();

		//Voir la grille_P1
		grille_P1.afficherTableau();



		//Mise en place du Joueur2
		System.out.println(text.M5);
		String saisieP2 = sc.next();	

		grille_P2 = new Tableau(DIMENSSION, saisieP2);
		grille_P2.insertBateau();
		
		//Choisir les coordonn�s de chaque bateau sur la grille_P1
		grille_P2.installation();

		//Voir la grille_P1
		grille_P2.afficherTableau();

		Ship hBateau = new Ship();
		
		do {
			//Au tour du Joueur1 
			unTour(grille_P1.getPlayer());
			//Au tour du Joueur2
			unTour(grille_P2.getPlayer());
		}while ((grille_P1.getShipList().contains(hBateau)) || (grille_P1.getShipList().contains(hBateau)));

	}





	public static void unTour(String Joueur) {
		Tableau saGrille;
		Tableau grilleAdversaire;
		Ship hBateau = new Ship();
		
		System.out.println("#AuTour " + Joueur);
		
		if (Joueur.equals(grille_P1.getPlayer())) {
			saGrille = grille_P1;
			grilleAdversaire = grille_P2;
		}
		else
			saGrille = grille_P2;
		grilleAdversaire = grille_P1;

		
		//Joueur1 
		nb_saisie= Message.questionF(text.M12);

		switch(nb_saisie) {
		case 1 : 
			do { 
				nb_saisie= Message.questionF(text.M15);
				switch(nb_saisie) {
				case 1 :
					for (Ship batcoll : saGrille.getShipList()) {
						if (batcoll instanceof Escorteur)
							hBateau = batcoll;
					}
					break;

				case 2:
					for (Ship batcoll : saGrille.getShipList()) {
						if (batcoll instanceof Croiseur)
							hBateau = batcoll;
					}
					break;

				case 3:
					for (Ship batcoll : saGrille.getShipList()) {
						if (batcoll instanceof SousMarin)
							hBateau = batcoll;
					}
					break;

				}

				System.out.println(text.M6);
				nb_saisie_x= sc.nextInt();

				System.out.println(text.M7);
				nb_saisie_y= sc.nextInt();

				nb_saisie= saGrille.moveBateau(hBateau, nb_saisie_x, nb_saisie_y);

				if (nb_saisie==1) {
					System.out.println(hBateau + text.M16);
				} else 
					System.out.println(hBateau + text.M13);
				
			}while (nb_saisie!=1);

			break;
			
			
			
		case 2:
			System.out.println(text.M6);
			nb_saisie_x= sc.nextInt();

			System.out.println(text.M7);
			nb_saisie_y= sc.nextInt();

			nb_saisie = grilleAdversaire.effectuerCoup(nb_saisie_x, nb_saisie_y);

			System.out.println(text.M14 + nb_saisie);
			break;
		default:
			System.out.println(text.M13);
		}
	}

}